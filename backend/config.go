package main

import "os"

var config map[string]string = make(map[string]string)

func loadConfig() {
	var frontendPort string
	var ok bool

	if frontendPort, ok = os.LookupEnv("FRONTEND_PORT"); !ok {
		frontendPort = "3001"
	}
	config["FRONTEND_PORT"] = frontendPort

}
