package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http/httptest"
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetAllRouteWhenEmptyTaskList(t *testing.T) {
	description := "fetch empty list when there are not tasks inserted"

	initStore(IN_MEMORY)
	app := Setup() // task set is empty

	req := httptest.NewRequest("GET", "/", nil)
	resp, _ := app.Test(req, 1)

	body, _ := ioutil.ReadAll(resp.Body)
	encodedBody := string(body)

	assert.Equalf(t, 200, resp.StatusCode, description)
	assert.Equalf(t, "[]", encodedBody, description)
}

func TestGetAllRoute(t *testing.T) {
	description := "fetch the tasks when there are tasks inserted"

	// Fixtures
	initStore(IN_MEMORY)
	app := Setup()
	taskStore.insertOne("test1")
	taskStore.insertOne("test2")

	req := httptest.NewRequest("GET", "/", nil)
	resp, _ := app.Test(req, 1)

	body, _ := ioutil.ReadAll(resp.Body)

	var decodedBody []string
	json.Unmarshal(body, &decodedBody)
	sort.Slice(decodedBody, func(i, j int) bool {
		return decodedBody[i] < decodedBody[j]
	})
	assert.Equalf(t, 200, resp.StatusCode, description)
	assert.Equalf(t, []string{"test1", "test2"}, decodedBody, description)
}

func TestCreateTaskRoute(t *testing.T) {

	description := "check that new tasks can be created"

	// Fixtures
	initStore(IN_MEMORY)
	app := Setup()

	testTask := map[string]string{"name": "test task"}
	reqBody, _ := json.Marshal(testTask)

	req := httptest.NewRequest("POST", "/task", bytes.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	resp, _ := app.Test(req, 1)

	body, _ := ioutil.ReadAll(resp.Body)

	var decodedBody string
	json.Unmarshal(body, &decodedBody)

	assert.Equalf(t, 200, resp.StatusCode, description)
	assert.Equalf(t, "Task inserted!", decodedBody, description)
}

func TestCreateTaskRouteAlreadyExists(t *testing.T) {
	description := "attempting to insert a duplicate task is an invalid operation"

	// Fixtures
	initStore(IN_MEMORY)
	dummyTask := "test task"
	app := Setup()
	taskStore.insertOne(dummyTask)

	testTask := map[string]string{"name": dummyTask}
	reqBody, _ := json.Marshal(testTask)

	req := httptest.NewRequest("POST", "/task", bytes.NewReader(reqBody))
	req.Header.Set("Content-Type", "application/json")
	resp, _ := app.Test(req, 1)

	body, _ := ioutil.ReadAll(resp.Body)

	var decodedBody string
	json.Unmarshal(body, &decodedBody)

	assert.Equalf(t, 409, resp.StatusCode, description)
	assert.Equalf(t, fmt.Sprintf("Task \"%s\" already exists!", dummyTask), decodedBody, description)
}
