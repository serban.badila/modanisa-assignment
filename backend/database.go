package main

import (
	"fmt"
	"os"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var (
	DBConn *gorm.DB
)

func initDatabase() *gorm.DB {
	var dbUser, dbPass, dbHost, dbPort string
	var ok bool

	if dbUser, ok = os.LookupEnv("DB_USER"); !ok {
		dbUser = "tasks"
	}

	if dbPass, ok = os.LookupEnv("DB_PASS"); !ok {
		dbPass = "pass"
	}

	if dbHost, ok = os.LookupEnv("DB_HOST"); !ok {
		dbHost = "localhost"
	}

	if dbPort, ok = os.LookupEnv("DB_PORT"); !ok {
		dbPort = "9920"
	}

	// setup the connection object
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=tasks port=%s sslmode=disable", dbHost, dbUser, dbPass, dbPort)
	DBConn, err := gorm.Open(sqlite.Open(dsn), &gorm.Config{})
	if err != nil {
		panic(fmt.Sprintf("Unable to establish database connection at %s:%s", dbHost, dbPort))
	}
	DBConn.AutoMigrate(&Task{})
	return DBConn
}
