module backend

go 1.17

require (
	github.com/gofiber/fiber/v2 v2.19.0
	github.com/stretchr/testify v1.7.0
	gorm.io/driver/sqlite v1.1.5
	gorm.io/gorm v1.21.15
)

require (
	github.com/andybalholm/brotli v1.0.2 // indirect
	github.com/davecgh/go-spew v1.1.0 // indirect
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.2 // indirect
	github.com/klauspost/compress v1.13.4 // indirect
	github.com/mattn/go-sqlite3 v1.14.8 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.29.0 // indirect
	github.com/valyala/tcplisten v1.0.0 // indirect
	golang.org/x/sys v0.0.0-20210514084401-e8d321eab015 // indirect
	gopkg.in/yaml.v3 v3.0.0-20200313102051-9f266ea9e77c // indirect
)
