package main

import (
	"errors"
	"fmt"

	"gorm.io/gorm"
)

var IN_MEMORY = "memory"
var DATABASE = "database"

var (
	taskStore Tasks
)

type Tasks interface {
	getAll() []string
	insertOne(name string) error
}

// implementations

type Task struct {
	Name string
}

type TasksInMemory struct {
	taskSet map[string]bool
}

type TasksRepository struct {
	dbConnection *gorm.DB
}

// methods

func (tm *TasksInMemory) insertOne(name string) error {
	if _, ok := tm.taskSet[name]; ok {
		return errors.New(fmt.Sprintf("Task \"%s\" already exists!", name))
	} else {
		tm.taskSet[name] = true
		return nil
	}
}

func (tm *TasksInMemory) getAll() []string {
	v := make([]string, 0)
	for key := range tm.taskSet {
		v = append(v, key)
	}
	return v
}

func (tm *TasksRepository) insertOne(name string) error {
	task := Task{
		Name: name,
	}
	var existingTasks []Task
	if result := tm.dbConnection.Where("name = ?", name).Find(&existingTasks); result.RowsAffected > 0 {
		return errors.New(fmt.Sprintf("Task \"%s\" already exists!", name))
	} else {
		tm.dbConnection.Create(&task)
		return nil
	}
}

func (tm *TasksRepository) getAll() []string {
	var v []Task
	tm.dbConnection.Find(&v)
	var names []string
	for _, t := range v {
		names = append(names, t.Name)
	}
	return names
}

// constructors

func MakeTasksInMemory() Tasks {
	tm := &TasksInMemory{
		make(map[string]bool, 0),
	}
	return tm
}

func MakeTasksRepository() Tasks {
	tm := &TasksRepository{
		dbConnection: initDatabase(),
	}
	return tm
}

func initStore(storeType string) {
	// Store builder
	switch store := storeType; store {
	case IN_MEMORY:
		taskStore = MakeTasksInMemory()
	case DATABASE:
		taskStore = MakeTasksRepository()
	default:
		panic("Unknown task storage type!")
	}
}
