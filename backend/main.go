package main

import (
	"github.com/gofiber/fiber/v2"
)

func main() {

	// initialize the data store and the app
	initStore(DATABASE)
	app := Setup()

	// start the server
	app.Listen(":3000")

}

func Setup() *fiber.App {
	// create the fiber app and register the handlers
	loadConfig()
	app := fiber.New()

	app.Get("/", getAll)
	app.Post("/task", createTask)
	app.Options("/task", options)

	return app
}
