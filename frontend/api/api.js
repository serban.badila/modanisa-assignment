import axios from 'axios';

const BASE_URL = "http://localhost:3000";

export async function fetchTasks () {
    const response = await axios.get(BASE_URL);
    if (!response.data){
        return []
    }
    return response.data.map((task, index) => ({
        id: index,
        name: task,
        isCompleted:  false
    }))
};

export async function createTask (taskName) { 
    return axios.post(`${BASE_URL}/task`, {'name': taskName});
}
