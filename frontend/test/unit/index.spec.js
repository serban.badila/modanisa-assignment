import { shallowMount } from '@vue/test-utils';
import index from '../../pages/index.vue';
import TaskList from '@/components/TaskList.vue'

describe('index.vue', () => {
  it('app is rendered', () => {
    const wrapper = shallowMount(index, {
      components: {
        'task-list': TaskList
      }
    });
    expect(wrapper.find('v-app').exists()).toBeTruthy()
  });

});