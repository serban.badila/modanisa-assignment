import { mount } from '@vue/test-utils'
import Task from '@/components/Task.vue'


describe('testing task', () => {
    const wrapper = mount(Task, {
        propsData: {
          injectedTaskName: "test task",
          isCompleted: false,
        }
      });  

  it('Find input- type text is properly set as a property', ()=>{
      expect(wrapper.find('v-checkbox').attributes('label')).toBe('test task')
  })
  it('has an icon', ()=>{
    expect(wrapper.find('v-icon').exists())
  })

  it('class is not completed by default', () => {
      expect(wrapper.find('v-checkbox').classes('default'))
  })

  it('class is completed by prop', () => {
    const wrapper = mount(Task, {
        propsData: {
          isCompleted: true,
        }
      }); 
    expect(wrapper.find('v-checkbox').classes('completed'))
  })
})