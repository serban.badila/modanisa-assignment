import { mount } from '@vue/test-utils'
import TaskList from '@/components/TaskList.vue'
import Task from '@/components/Task.vue'


const mockTasks = [
  { name: 'test1', id: '123' },
  { name: 'test2', id: '1234' }
]


describe('testing the task list', () => {
  const wrapper = mount(TaskList, {
    data() {
      return {
        Tasks: mockTasks
      }
    }
  })
  it('checks all tasks are rendered', async () => {
    expect(wrapper.find('#todo-list').exists()).toBeTruthy();
    expect(wrapper.findAllComponents(Task).length).toBe(2);

  });

  it('checks the task list exists', async () => {
    expect(wrapper.find('ul').attributes('id')).toBe('todo-list');
  });

})