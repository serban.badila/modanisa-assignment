import 'regenerator-runtime/runtime'
import Vue from 'vue';

Vue.config.ignoredElements = [
    'v-app', 'v-checkbox', 'v-layout', 'v-icon', 'v-btn', 'v-col', 'v-row', 'v-container', 'v-text-field'
];
